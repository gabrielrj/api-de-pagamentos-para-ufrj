<?php

namespace App\Providers;

use App\Http\Services\BanksPaymentServices\BancoDoBrasil\PaymentSlipGeneratorServiceBB;
use App\Interfaces\Services\BancoDoBrasil\PaymentSlipGeneratorBBInterface;
use App\Interfaces\Services\PaymentSlipBBServiceInterface;
use App\Services\BancoDoBrasil\PaymentSlipBBService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(PaymentSlipBBServiceInterface::class, PaymentSlipBBService::class);
    }
}
