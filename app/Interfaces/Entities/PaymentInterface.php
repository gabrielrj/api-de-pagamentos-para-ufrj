<?php

namespace App\Interfaces\Entities;

interface PaymentInterface
{
    public function getType() : string;

    public function getBank() : string;
}
