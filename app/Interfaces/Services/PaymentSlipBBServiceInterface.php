<?php

namespace App\Interfaces\Services;

use App\Interfaces\Factories\PaymentSlipBBFactoryInterface;
use App\Interfaces\Repositories\BancoDoBrasil\PaymentSlipBBRepositoryInterface;

interface PaymentSlipBBServiceInterface extends PaymentServiceInterface
{
    public function __construct(PaymentSlipBBRepositoryInterface $paymentSlipBBRepository, PaymentSlipBBFactoryInterface $paymentSlipBBFactory);

    public function print_second_copy_of_payment_slip();
}
