<?php

namespace App\Exceptions;

use Exception;

class PaymentNotFoundException extends Exception
{
    protected $message = 'Tipo de transação (pagamento) não encontrado na nossa base de dados.';
}
