<?php

namespace App\Models\Billing\Enums;

/**
 * Tipo de Pessoa para o qual foi gerada a cobrança
 */
abstract class PersonType
{
    const FISICA = '1';
    const JURIDICA = '2';
}
