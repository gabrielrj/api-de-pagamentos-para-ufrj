<?php

namespace App\Exceptions;

use Exception;

class PaymentIntegrationBankException extends Exception
{
    protected $message = 'Ocorreu um erro na operação de integração e interface com o banco gerador/processador do pagamento.';
}
