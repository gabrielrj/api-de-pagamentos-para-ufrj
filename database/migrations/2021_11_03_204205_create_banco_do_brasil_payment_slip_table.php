<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancoDoBrasilPaymentSlipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banco_do_brasil_payment_slip', function (Blueprint $table) {
            $table->id();
            $table->foreignId('billing_id')->comment('Chave estrangeira que identifica a cobrança que gerou esse boleto.')->constrained('billings');
            $table->string('bank_slip_id', 17)->comment('Código de identificação do boleto no banco gerador. (Obrigatório)');
            $table->integer('ecommerce_agreement_id')->nullable()->comment('Id do convênio de comércio eletrônico, caso haja. (Opcional)');
            $table->integer('billing_agreement_id')->nullable()->comment('Id do convênio de cobrança, caso haja. (Opcional)');
            $table->char('duplicate_type', 2)->nullable()->comment('Tipo de título que originará o boleto. {DM - Duplicata Mercantil | DS - Duplicata Serviço} (Opcional)');
            $table->decimal('amount_paid')->nullable()->comment('Valor que foi pago.');
            $table->string('shop_msg', 1082)->nullable()->comment('Instruções do beneficiário, que serão apresentadas: Integralmente na parte superior da página do boleto; Até os primeiros 480 caracteres na ficha de compensação do boleto no campo “Informações de responsabilidade da beneficiária”.');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banco_do_brasil_payment_slip');
    }
}
