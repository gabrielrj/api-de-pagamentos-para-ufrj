<?php

namespace App\Http\Controllers\BancoDoBrasil;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ResponseController;
use App\Models\Enums\PaymentServiceType;
use App\Models\Enums\PaymentType;
use App\Services\PaymentServiceMediator;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    use ResponseController;

    private $paymentService;


    public function __construct()
    {
        $this->paymentService = new PaymentServiceMediator(PaymentType::PAYMENTSLIP, PaymentServiceType::BANCODOBRASIL);
    }

    /**
     * Gera o boleto do Banco do Brasil
     *
     * @param Request $request
     */
    public function generate_payment_slip(Request $request)
    {

        return $this->run('Gerar boleto do Banco do Brasil', function () use($request){

        });
    }
}
