<?php

namespace App\Interfaces\Services;

use App\Interfaces\Entities\PaymentInterface;
use App\Models\Billing\Billing;

interface PaymentServiceInterface
{

    /**
     * Método responsável por executar uma transação no serviço de pagamento para um tipo de pagamento específico.
     *
     * @param array $params
     * @param int $billingId
     * @return mixed
     */
    public function create_payment(array $params, int $billingId);

    /**
     * Método responsável por cancelar a transação.
     *
     * @param int $billingId
     * @return mixed
     */
    public function cancel_transaction(int $billingId);

    /**
     * Método responsável por realizar a transação financeira (gerar boleto, garantir interface com Banco de pagamento para realizar transações, etc)
     *
     * @param Billing $billing
     * @param PaymentInterface $payment
     * @param array|null $params
     * @return mixed
     */
    public function perform_transaction(Billing $billing,
                                        PaymentInterface $payment,
                                        array $params = null);
}
