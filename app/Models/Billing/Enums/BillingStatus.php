<?php

namespace App\Models\Billing\Enums;

abstract class BillingStatus
{
    /**
     * Cobrança já foi gerada e está pendente de pagamento.
     */
    const PENDING = 'pending';

    /**
     * Cobrança já foi quitada/liquidada/paga.
     */
    const LIQUIDATED = 'liquidated';

    /**
     * Cobrança foi cancelada.
     */
    const CANCELED = 'canceled';
}
