<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_app_id')->comment('Chave estrangeira que identifica o sistema cliente que solicitou a geração do boleto.')->constrained('client_applications');
            $table->enum('status', ['pending', 'liquidated', 'canceled'])->comment('Status/situação do pagamento. {Pendente, Liquidado ou Cancelado}. (Obrigatório)');
            $table->char('payment_method', 1)->comment('Meio de pagamento. {1 - Boleto}. (Obrigatório)');
            $table->char('person_type',1)->comment('Indicador do tipo de pessoa: 1 - Pessoa física | 2 - Pessoa jurídica');
            $table->decimal('amount')->comment('Valor da cobrança. (Obrigatório)');
            $table->decimal('prepayment_discount')->nullable()->comment('Valor do desconto a ser dado a uma cobrança por pagamento antes da data limite. (Obrigatório caso haja data limite/prazo de desconto)');
            $table->date('due_date')->comment('Prazo para quitação da cobrança. (Obrigatório)');
            $table->date('deadline_payment_discount')->nullable()->comment('Data limite/Prazo para desconto no pagamento. (Obrigatório caso haja valor de desconto)');
            $table->date('payment_date')->nullable()->comment('Data de quitação da cobrança, caso haja. (Opcional)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
