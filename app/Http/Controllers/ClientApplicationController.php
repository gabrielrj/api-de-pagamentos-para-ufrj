<?php

namespace App\Http\Controllers;

use App\Exceptions\ClientApplicationNotFoundException;
use App\Http\Controllers\Traits\ResponseController;
use App\Http\Resources\ClientApplicationResource;
use App\Services\ClientApplicationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ClientApplicationController extends Controller
{
    use ResponseController;

    private $clientApplicationService;

    public function __construct()
    {
        $this->clientApplicationService = App::make(ClientApplicationService::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return ClientApplicationResource
     */
    public function index(): ClientApplicationResource
    {
        return $this->run('Lista todos os client systems cadastrados', function(){
            return new ClientApplicationResource($this->clientApplicationService->get_all());
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return ClientApplicationResource
     */
    public function store(Request $request): ClientApplicationResource
    {
        return $this->run('Cadastrar client app', function () use($request) {
            $client_app_cadastrado = $this->clientApplicationService->store($request->toArray());

            return \response()->json([
                'client_app_stored' => isset($client_app_cadastrado),
                'msg_status' => 'Client app cadastrado com sucesso.',
                'request_status' => 'success'
            ]);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return ClientApplicationResource
     * @throws \Throwable
     */
    public function show($id): ClientApplicationResource
    {
        return $this->run('Busca de client app por id', function () use($id){
            $result = $this->clientApplicationService->find_for_id($id);

            throw_unless(isset($result), ClientApplicationNotFoundException::class);

            return new ClientApplicationResource($result);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return ClientApplicationResource
     */
    public function update(Request $request, $id): ClientApplicationResource
    {
        return $this->run('Altera dados do client app', function () use($request, $id){
            $client_app_alterado = $this->clientApplicationService->update($request->toArray(), $id);

            return \response()->json([
                'client_app_updated' => isset($client_app_alterado),
                'msg_status' => 'Client app alterado com sucesso.',
                'request_status' => 'success'
            ]);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     */
    public function destroy($id)
    {
        return $this->run('Exclui client app', function () use($id){
            return \response()->json([
                'client_app_deleted' => $this->clientApplicationService->delete($id),
                'msg_status' => 'Client app excluído com sucesso.',
                'request_status' => 'success'
            ]);
        });
    }
}
