<?php

namespace App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip;

use App\Interfaces\Entities\PaymentInterface;
use App\Models\Billing\Billing;
use App\Models\Enums\PaymentServiceType;
use App\Models\Enums\PaymentType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSlipBB extends Model implements PaymentInterface
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'banco_do_brasil_payment_slips';

    protected $hidden = ['id', 'billing_id'];

    protected $fillable = [
        'billing_id',
        'bank_slip_id',
        'ecommerce_agreement_id',
        'billing_agreement_id',
        'duplicate_type',
        'amount_paid',
        'shop_msg'
    ];

    public function billing(){
        return $this->belongsTo(Billing::class, 'billing_id');
    }

    public function getType() : string
    {
        return PaymentType::PAYMENTSLIP;
    }

    public function getBank(): string
    {
        return PaymentServiceType::BANCODOBRASIL;
    }
}
