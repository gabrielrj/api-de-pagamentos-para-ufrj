<?php

namespace App\Models\Billing;

use App\Interfaces\Factories\BillingFactoryInterface;
use App\Models\Billing\Enums\BillingStatus;

class BillingFactory implements BillingFactoryInterface
{

    public function getBilling(array $params) : Billing
    {
        $billing = new Billing();

        $billing->client_app_id = $params['client_app_id'];
        $billing->status = BillingStatus::PENDING;
        $billing->payment_method = $params['payment_method'];
        $billing->person_type = $params['person_type'];
        $billing->amount = $params['amount'];
        $billing->discount = $params['discount'];
        $billing->due_date = $params['due_date'];
        $billing->deadline_payment_discount = $params['deadline_payment_discount'];
        $billing->payment_date  = $params['payment_date'];

        return $billing;
    }
}
