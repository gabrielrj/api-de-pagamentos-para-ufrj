<?php

namespace App\Exceptions;

use Exception;

class ClientApplicationUpdateException extends Exception
{
    protected $message = 'Ocorreu um erro ao tentar alterar o sistema (client application) solicitado.';
}
