<?php

namespace App\Services;

use App\Exceptions\ClientApplicationDeleteException;
use App\Exceptions\ClientApplicationNotFoundException;
use App\Exceptions\ClientApplicationUpdateException;
use App\Interfaces\Repositories\ClientApplicationRepositoryInterface;
use App\Models\ClientApplication;

class ClientApplicationService
{
    private $repository;

    public function __construct(ClientApplicationRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function get_all()
    {
        return $this->repository->newQuery()->get();
    }


    public function store(array $params)
    {
        return $this->repository->create($params);
    }


    public function find_for_id($id) : ?ClientApplication
    {
        return $this->repository->newQuery()
            ->whereClientId($id)
            ->first();
    }


    /**
     * @throws \Throwable
     */
    public function update(array $params, $id)
    {
        $client_app = $this->find_for_id($id);

        throw_unless(isset($client_app), ClientApplicationNotFoundException::class);

        return throw_unless($this->repository->update($client_app, $params), ClientApplicationUpdateException::class);
    }


    /**
     * @throws \Throwable
     */
    public function delete($id)
    {
        $client_app = $this->find_for_id($id);

        throw_unless(isset($client_app), ClientApplicationNotFoundException::class);

        return throw_unless($this->repository->delete($client_app), ClientApplicationDeleteException::class);
    }
}
