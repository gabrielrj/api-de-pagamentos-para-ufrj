<?php

namespace App\Exceptions;

use Exception;

class ClientApplicationCreateException extends Exception
{
    protected $message = 'Ocorreu um erro ao tentar cadastrar o sistema (client application).';
}
