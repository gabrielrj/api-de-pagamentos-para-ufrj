<?php

namespace App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\Enums;

abstract class DuplicateType
{
    /**
     * DM : Duplicata Mercantil – utilizado quando forem vendidas mercadorias/produtos;
     */
    const DM = 'DM';

    /**
     * DS : Duplicata de serviços – quando a loja virtual vender a prestação de serviços.
     */
    const DS = 'DS';
}
