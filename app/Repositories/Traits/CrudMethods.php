<?php

namespace App\Repositories\Traits;

use Illuminate\Database\Eloquent\Model;

trait CrudMethods
{

    /**
     * @param array $data
     *
     * @return Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create(array $data = []): Model
    {
        return $this->newQuery()->create($data);
    }

    /**
     * .
     *
     * @param Model $model
     *
     * @return bool
     */
    public function save(Model $model): bool
    {
        return $model->save();
    }

    /**
     *
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data = []): bool
    {
        return $model->update($data);
    }

    /**
     * .
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    public function forceDelete(Model $model): bool
    {
        return $model->forceDelete();
    }
}
