<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    use HasFactory;

    protected $hidden = ['id'];

    protected $fillable = [
        'client_app_id',
        'status',
        'payment_method',
        'person_type',
        'amount',
        'prepayment_discount',
        'due_date',
        'deadline_payment_discount',
        'payment_date',
    ];
}
