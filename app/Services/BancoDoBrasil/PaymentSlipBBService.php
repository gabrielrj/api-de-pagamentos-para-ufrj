<?php

namespace App\Services\BancoDoBrasil;

//Exceptions
use App\Exceptions\PaymentCreateException;

//Interfaces
use App\Interfaces\Entities\PaymentInterface;
use App\Interfaces\Factories\PaymentSlipBBFactoryInterface;
use App\Interfaces\Repositories\BancoDoBrasil\PaymentSlipBBRepositoryInterface;

//Models
use App\Models\Billing\Billing;
use App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\Enums\BBPaymentSlipOption;
use App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\Enums\DuplicateType;
use App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\PaymentSlipBB;

//Helpers
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Nette\NotImplementedException;


class PaymentSlipBBService implements \App\Interfaces\Services\PaymentSlipBBServiceInterface
{

    private $paymentRepository;
    private $paymentSlipBBFactory;
    private $urlService;

    public function __construct(PaymentSlipBBRepositoryInterface $paymentSlipBBRepository,
                                PaymentSlipBBFactoryInterface $paymentSlipBBFactory)
    {
        $this->paymentRepository = $paymentSlipBBRepository;
        $this->paymentSlipBBFactory = $paymentSlipBBFactory;
    }

    /**
     * @param mixed $urlService
     */
    public function setUrlService($urlService): void
    {
        $this->urlService = $urlService;
    }

    /**
     * @inheritDoc
     * @throws PaymentCreateException
     * @throws \Throwable
     */
    public function create_payment(array $params, int $billingId)
    {
        $newPayment = $this->paymentSlipBBFactory->getPaymentSlip($params, $billingId);

        throw_unless($this->paymentRepository->save($newPayment), PaymentCreateException::class);
    }

    /**
     * @inheritDoc
     */
    public function cancel_transaction(int $billingId)
    {
        // TODO: Implement cancel_transaction() method.
    }

    /**
     * @inheritDoc
     */
    public function perform_transaction(Billing $billing,
                                        PaymentInterface $payment,
                                        array $params = null)
    {
        $params_to_send_bb_service = $this->mount_array_params_for_send_to_banco_do_brasil_service($billing,
            $payment,
            BBPaymentSlipOption::FirstCopy,
            $params);

        if(!isset($this->urlService)){
            $response = $this->establish_connection_with_bank_service($params_to_send_bb_service, 'post');

            dd($response);
        }else{
            throw new NotImplementedException('Não há implementação para geração de boleto do Banco do Brasil fora do serviço MPAG.');
        }
    }

    public function print_second_copy_of_payment_slip()
    {
        // TODO: Implement print_second_copy_of_payment_slip() method.
    }

    /**
     * * Método: Formata os valores de data/hora no padrão do BB (DDMMYYYY) sem barras e em Br Date.
     *
     * @param $value
     * @return string
     */
    private function format_date_params($value): string
    {
        return (Str::length($value) == 10 ? Carbon::createFromFormat('Y-m-d', $value)->format('dmY') : Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('dmY'));
    }

    /**
     * Método: Formata os valores monetários no padrão do BB (NNNNN) sem ponto e nem vírgula.
     *
     * @param $value
     * @return string
     */
    private function format_caracteres_to_money_value($value): string
    {
        return (string) ($value < 1.00 ? (int)str_replace(['.', ','], '', $value) : str_replace(['.', ','], '', $value));
    }

    /**
     * Método: Cria vetor com os parâmetros para envio ao serviço de cobrança/geração de boleto do BB - Banco do Brasil.
     * Retorno: array
     *
     * @param Billing $billing
     * @param PaymentSlipBB $payment
     * @param BBPaymentSlipOption $option
     * @param array $params
     * @return array
     */
    private function mount_array_params_for_send_to_banco_do_brasil_service(Billing $billing,
                                                                            PaymentSlipBB $payment,
                                                                            BBPaymentSlipOption $option,
                                                                            array $params): array
    {
        return [
            'idConv' => $payment->ecommerce_agreement_id,
            'refTran' => $payment->bank_slip_id,
            'valor' => $this->format_caracteres_to_money_value($billing->amount),
            'tpPagamento' => $option,
            'qtdPontos' => '0',
            'dtVenc' => $this->format_date_params($billing->due_date),
            'cpfCnpj' => Str::replace(['-', '.'], '', $params['cpf_cnpj_cliente']),
            'indicadorPessoa' => $billing->person_type,
            'valor desconto' => $billing->prepayment_discount != null ? $this->format_caracteres_to_money_value($billing->prepayment_discount) : null,
            'dataLimiteDesconto' => $billing->deadline_payment_discount != null ? $this->format_date_params($billing->deadline_payment_discount) : null,
            'tpDuplicata' => $payment->duplicate_type ??= DuplicateType::DS,
            'urlRetorno' => 'http://www.teste.ufrj.br/impressao',
            'urlInforma' => '',
            'nome' => $params['nome_cliente'],
            'endereco' => Str::replaceArray('?', [$params['logradouro_cliente'], $params['numero_cliente'], ((Arr::has($params, 'complemento_cliente') && $params['complemento_cliente']) ? $params['complemento_cliente'] : 's/n'), $params['bairro_cliente']], '?, ?, ? - ?'),
            'cidade' => $params['cidade_cliente'],
            'uf' => $params['uf_cliente'],
            'cep' => Str::replace(['-', '.'], '', $params['cep_cliente']),
            'msgLoja' => $payment->shop_msg,
        ];
    }

    /**
     * Método: Estabele conexão com a URL de serviço do Banco do Brasil e executa método específico.
     *
     * @param array $params
     * @param string $method
     * @return \Illuminate\Http\Client\Response|null
     */
    private function establish_connection_with_bank_service(array $params, string $method = 'get'): ?\Illuminate\Http\Client\Response
    {
        switch ($method){
            case 'get':
                return Http::get($this->urlService, $params);
            case 'post':
                return Http::post($this->urlService, $params);
            default:
                return null;
        }
    }
}
