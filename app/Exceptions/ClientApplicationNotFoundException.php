<?php

namespace App\Exceptions;

use Exception;

class ClientApplicationNotFoundException extends Exception
{
    protected $message = 'Não foi possível encontrar o sistema(client application) solicitado na nossa base de dados.';
}
