<?php

namespace App\Interfaces\Factories;

use App\Models\Billing\Billing;

interface BillingFactoryInterface
{
    /**
     * Método responsável por fabricar uma cobrança e retorna-la para ser salva pelo repositório;
     *
     * @param array $params
     * @return Billing
     */
    public function getBilling(array $params): Billing;
}
