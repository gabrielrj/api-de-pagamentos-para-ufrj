<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ClientApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->client_id,
            'name' => $this->app_name,
            'data_cadastro' => Carbon::createFromFormat('Y-m-d', $this->created_at)->format('d/m/Y H:i'),
            'data_exclusao' => isset($this->deleted_at) ? Carbon::createFromFormat('Y-m-d', $this->created_at)->format('d/m/Y H:i') : null,
            'status' => $this->status
        ];
    }
}
