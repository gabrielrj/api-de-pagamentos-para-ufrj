<?php

namespace App\Repositories\BancoDoBrasil;

use App\Interfaces\Repositories\AbstractRepository;
use App\Interfaces\Repositories\BancoDoBrasil\PaymentSlipBBRepositoryInterface;
use App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\PaymentSlipBB;
use App\Repositories\Traits\CrudMethods;

class PaymentSlipBBRepository extends AbstractRepository implements PaymentSlipBBRepositoryInterface
{
    use CrudMethods;

    protected $modelClass = PaymentSlipBB::class;
}
