<?php

namespace App\Exceptions;

use Exception;

class PaymentCreateException extends Exception
{
    protected $message = 'Ocorreu um erro ao tentar registrar o tipo de pagamento que será efetuado.';
}
