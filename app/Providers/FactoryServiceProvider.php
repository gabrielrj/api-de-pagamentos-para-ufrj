<?php

namespace App\Providers;

use App\Interfaces\Factories\BillingFactoryInterface;
use App\Interfaces\Factories\PaymentSlipBBFactoryInterface;
use App\Models\Billing\BillingFactory;
use App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\PaymentSlipBBFactory;
use Illuminate\Support\ServiceProvider;

class FactoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BillingFactoryInterface::class, BillingFactory::class);
        $this->app->bind(PaymentSlipBBFactoryInterface::class, PaymentSlipBBFactory::class);
    }
}
