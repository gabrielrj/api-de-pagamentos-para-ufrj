<?php

namespace App\Http\Controllers\Traits;

trait ResponseController
{
    public function run($actionName, callable $fn){
        try {
            return $fn();
        }catch (\Exception $exception){
            $errors = [
                'error' =>
                    [
                        'action_name' => $actionName,
                        'exception_type' => gettype($exception),
                        'exception_message' => $exception->getMessage(),
                        'exception_trace' => $exception->getTraceAsString(),
                        'exception_code' => $exception->getCode()
                    ]
            ];

            return response()->json($errors, 422)->withHeaders($errors);
        }
    }
}
