<?php

namespace App\Models\Enums;

abstract class PaymentType
{
    const PAYMENTSLIP = 'paymentslip';
    const CREDITCARD = 'creditcard';
    const DEBITCARD = 'debitcard';
    const PIX = 'pix';
    const OTHER = 'other';
}
