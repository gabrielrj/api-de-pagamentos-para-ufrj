<?php

namespace App\Exceptions;

use Exception;

class BillingNotFoundException extends Exception
{
    protected $message = 'Não foi possível encontrar o registro de cobrança solicitado em nossa base de dados.';
}
