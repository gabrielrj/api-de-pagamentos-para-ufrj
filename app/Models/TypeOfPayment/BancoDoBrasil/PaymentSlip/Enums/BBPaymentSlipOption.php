<?php

namespace App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip\Enums;

abstract class BBPaymentSlipOption
{
    /**
     * Primeira Via (geração/emissão)
     */
    const FirstCopy = '2';

    /**
     * Segunda via (reimpressão)
     */
    const SecondCopy = '21';
}
