<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ClientApplication extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'client_id',
        'app_name'
    ];

    protected $attributes = [
        'client_id' => (string) Str::uuid(),
    ];

    protected $appends = [
        'status',
    ];

    public function getStatusAttribute(): string
    {
        return isset($this->deleted_at) ? 'ativo' : 'inativo';
    }
}
