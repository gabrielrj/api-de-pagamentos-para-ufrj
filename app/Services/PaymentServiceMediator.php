<?php

namespace App\Services;

use App\Exceptions\PaymentIntegrationBankException;
use App\Interfaces\Entities\PaymentInterface;
use App\Interfaces\Services\PaymentSlipBBServiceInterface;
use App\Models\Billing\Billing;
use App\Models\Enums\PaymentServiceType;
use App\Models\Enums\PaymentType;
use App\Services\BancoDoBrasil\PaymentSlipBBService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class PaymentServiceMediator implements \App\Interfaces\Services\PaymentServiceInterface
{
    private $paymentService;
    private $billingService;

    /**
     * É necessário especificar o paymentType (Tipo de Transação/Pagamento, ex: Boleto, Pix, Cartão de Crédito, etc...)
     * e o paymentServiceType (Tipo de Serviço de Pagamento, ex: Banco do Brasil, Bradesco, Pagseguro, etc...).
     *
     * @param PaymentType $paymentType
     * @param PaymentServiceType $paymentServiceType
     */
    public function __construct(PaymentType $paymentType, PaymentServiceType $paymentServiceType)
    {
        if($paymentType == PaymentType::PAYMENTSLIP){
            if($paymentServiceType == PaymentServiceType::BANCODOBRASIL) {
                $this->paymentService = App::make(PaymentSlipBBServiceInterface::class);
                $this->paymentService->setUrlService(env('BB_URL_SERVICE'));
            }
        }

        $this->billingService = App::make(BillingService::class);
    }

    /**
     * @inheritDoc
     */
    public function create_payment(array $params, int $billingId)
    {
        return $this->paymentService->create_payment($params, $billingId);
    }

    /**
     * @inheritDoc
     */
    public function cancel_transaction(int $billingId)
    {
        return $this->paymentService->cancel_transaction($billingId);
    }

    /**
     * @inheritDoc
     */
    public function perform_transaction(Billing $billing,
                                        PaymentInterface $payment,
                                        array $params = null)
    {
        return $this->paymentService->perform_transaction($billing, $payment, $params);
    }

    /**
     * Registra a cobrança, cadastra o pagamento e gera a transação.
     *
     * @param array $params
     * @return mixed
     * @throws PaymentIntegrationBankException
     */
    public function create_payment_and_perform_transaction(array $params){
        DB::beginTransaction();

        try {
            $billing = $this->billingService->create_billing($params);

            $payment = $this->create_payment($params, $billing->id);

            $response = $this->perform_transaction($billing, $payment, $params);

            if($response) {
                DB::commit();

                return $response;
            }else
                throw new PaymentIntegrationBankException();
        }catch (\Exception $exception){
            DB::rollBack();

            throw $exception;
        }
    }
}
