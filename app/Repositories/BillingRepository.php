<?php

namespace App\Repositories;

use App\Interfaces\Repositories\AbstractRepository;
use App\Models\Billing\Billing;
use App\Repositories\Traits\CrudMethods;

class BillingRepository extends AbstractRepository
{
    use CrudMethods;

    protected $modelClass = Billing::class;
}
