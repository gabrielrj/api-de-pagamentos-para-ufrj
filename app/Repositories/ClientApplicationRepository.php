<?php

namespace App\Repositories;

use App\Interfaces\Repositories\AbstractRepository;
use App\Models\ClientApplication;
use App\Repositories\Traits\CrudMethods;

class ClientApplicationRepository extends AbstractRepository
{
    use CrudMethods;

    protected $modelClass = ClientApplication::class;
}
