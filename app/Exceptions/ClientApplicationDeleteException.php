<?php

namespace App\Exceptions;

use Exception;

class ClientApplicationDeleteException extends Exception
{
    protected $message = 'Ocorreu um erro ao tentar excluir o sistema (client application) solicitado.';
}
