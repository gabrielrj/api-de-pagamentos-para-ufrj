<?php

namespace App\Interfaces\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Retorna o queryBuilder respectivo ao modelclass do repositório.
     *
     * @return Builder
     */
    function newQuery() : Builder;

    /**
     * Cria um objeto Model com as informações do parâmetro $data.
     *
     * @param array $data
     *
     * @return Model
     */
    function create(array $data = []): Model;

    /**
     * Atualiza a model enviada como parâmetro com os dados do array de $data.
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    function update(Model $model, array $data = []): bool;

    /**
     * Executa o método de salvamento do modelo, permitindo implementação anterior da lógica de negócio.
     *
     * @param Model $model
     *
     * @return bool
     */
    function save(Model $model): bool;

    /**
     * Executa o método de exclusão lógica do modelo, permitindo implementação anterior da lógica de negócio.
     *
     * @param Model $model
     *
     * @return bool
     */
    function delete(Model $model): bool;


    /**
     * Executa o método de exclusão física do modelo enviado.
     *
     * @param Model $model
     * @return bool
     *
     */
    function forceDelete(Model $model): bool;
}
