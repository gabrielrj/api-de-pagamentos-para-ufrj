<?php

namespace App\Interfaces\Repositories\BancoDoBrasil;

use App\Interfaces\Repositories\PaymentSlipRepositoryInterface;

interface PaymentSlipBBRepositoryInterface extends PaymentSlipRepositoryInterface
{

}
