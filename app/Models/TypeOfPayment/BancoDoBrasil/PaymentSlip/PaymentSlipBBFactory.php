<?php

namespace App\Models\TypeOfPayment\BancoDoBrasil\PaymentSlip;


use App\Interfaces\Entities\PaymentInterface;
use App\Interfaces\Factories\PaymentSlipBBFactoryInterface;

class PaymentSlipBBFactory implements PaymentSlipBBFactoryInterface
{

    public function getPaymentSlip(array $params, int $billingId): PaymentInterface
    {
        $bancoDoBrasilPaymentSlip = new PaymentSlipBB();

        $bancoDoBrasilPaymentSlip->billing_id = $billingId;
        $bancoDoBrasilPaymentSlip->bank_slip_id = $params['bank_slip_id'];
        $bancoDoBrasilPaymentSlip->ecommerce_agreement_id = $params['ecommerce_agreement_id'];
        $bancoDoBrasilPaymentSlip->billing_agreement_id = $params['billing_agreement_id'];
        $bancoDoBrasilPaymentSlip->duplicate_type = $params['duplicate_type'];
        $bancoDoBrasilPaymentSlip->amount_paid = $params['amount_paid'];

        return $bancoDoBrasilPaymentSlip;
    }
}
