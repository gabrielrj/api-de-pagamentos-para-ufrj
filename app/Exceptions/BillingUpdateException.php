<?php

namespace App\Exceptions;

use Exception;

class BillingUpdateException extends Exception
{
    protected $message = 'Ocorreu um erro ao tentar cadastrar um novo registro de cobrança.';
}
