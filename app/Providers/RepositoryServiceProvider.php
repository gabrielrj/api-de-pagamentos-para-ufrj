<?php

namespace App\Providers;

use App\Interfaces\Repositories\BancoDoBrasil\PaymentSlipBBRepositoryInterface;
use App\Interfaces\Repositories\BillingRepositoryInterface;
use App\Interfaces\Repositories\ClientApplicationRepositoryInterface;
use App\Interfaces\Repositories\PaymentSlipRepositoryInterface;
use App\Repositories\BancoDoBrasil\PaymentSlipBBRepository;
use App\Repositories\BillingRepository;
use App\Repositories\ClientApplicationRepository;
use App\Repositories\PaymentSlipRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ClientApplicationRepositoryInterface::class, ClientApplicationRepository::class);
        $this->app->bind(BillingRepositoryInterface::class, BillingRepository::class);

        //PaymentSlips Repositories (for Banks)
        $this->app->bind(PaymentSlipBBRepositoryInterface::class, PaymentSlipBBRepository::class);
    }
}
