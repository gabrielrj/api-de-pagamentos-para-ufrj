<?php

namespace App\Services;

use App\Exceptions\BillingCreateException;
use App\Exceptions\BillingDueDateInvalidException;
use App\Exceptions\BillingNotFoundException;
use App\Exceptions\BillingUpdateException;
use App\Interfaces\Factories\BillingFactoryInterface;
use App\Interfaces\Repositories\BillingRepositoryInterface;
use App\Models\Billing\Billing;
use App\Models\Billing\Enums\BillingStatus;
use Illuminate\Support\Carbon;

class BillingService
{
    private $billingRepository;
    private $billingFactory;

    public function __construct(BillingRepositoryInterface $repository,
                                BillingFactoryInterface $billingFactory)
    {
        $this->billingRepository = $repository;
        $this->billingFactory = $billingFactory;
    }

    /**
     *
     * @param array $params
     * @return Billing
     * @throws \Exception
     * @throws \Throwable
     */
    public function create_billing(array $params): Billing
    {
        $new_billing = $this->billingFactory->getBilling($params);

        //Data de vencimento da cobrança não pode ser menor/igual a data de hoje.
        throw_unless($this->valide_rule_for_due_date_must_be_greater_than_todays_date($new_billing->due_date),
            new BillingDueDateInvalidException('A data de vencimento da cobrança não pode ser menor que a data de hoje.'));

        //Data de limite para desconto não pode ser menor/igual a data de hoje.
        throw_unless($this->valide_rule_for_deadline_payment_discount_must_be_greater_than_todays_date($new_billing->deadline_payment_discount),
            new BillingDueDateInvalidException('A data de limite para pagamento da cobrança com desconto não pode ser menor que a data de hoje.'));

        //Data de limite para desconto não pode ser maior que a data de vencimento da cobrança.
        throw_unless($this->valide_rule_for_deadline_payment_discount_must_be_less_than_or_equal_to_due_date($new_billing->deadline_payment_discount, $new_billing->due_date),
            new BillingDueDateInvalidException('A data de limite para pagamento da cobrança com desconto não pode ser maior que a data de vencimento da cobrança.'));

        return throw_unless($this->billingRepository->save($new_billing), new BillingCreateException());

    }

    /**
     * Método responsável por cancelar a cobrança
     *
     * @param $billingId
     * @throws \Exception
     * @throws \Throwable
     */
    public function cancel_billing($billingId){
        $billing = $this->billingRepository->newQuery()->find($billingId);

        throw_unless(isset($billing), BillingNotFoundException::class);

        $billing->status = BillingStatus::CANCELED;

        return throw_unless($this->billingRepository->save($billing), new BillingUpdateException());
    }

    /**
     * Método responsável por realizar a liquidação da cobrança
     *
     * @param $billingId
     * @throws \Exception
     * @throws \Throwable
     */
    public function settle_billing($billingId){
        $billing = $this->billingRepository->newQuery()->find($billingId);

        $billing->status = BillingStatus::LIQUIDATED;

        throw_unless(isset($billing), BillingNotFoundException::class);

        $billing->status = BillingStatus::CANCELED;

        return throw_unless($this->billingRepository->save($billing), new BillingUpdateException());
    }


    /* Rules **/
    //Create billing
    /**
     * Validação de regra
     * Método: Create billing
     * Regra: Data de vencimento precisa ser maior que a data de hoje.
     * Retorno: boleano (true or false)
     *
     * @param $dueDate
     * @return bool
     */
    private function valide_rule_for_due_date_must_be_greater_than_todays_date($dueDate): bool
    {
        return (Carbon::createFromFormat('Y-m-d', $dueDate)->getTimestamp() > now()->getTimestamp());
    }

    /**
     * Validação de regra
     * Método: Create billing
     * Regra: Data de limite para desconto precisa ser maior que a data de hoje.
     * Retorno: boleano (true or false)
     *
     * @param $deadlinePaymentDiscount
     * @return bool
     */
    private function valide_rule_for_deadline_payment_discount_must_be_greater_than_todays_date($deadlinePaymentDiscount): bool
    {
        return (Carbon::createFromFormat('Y-m-d', $deadlinePaymentDiscount)->getTimestamp() > now()->getTimestamp());
    }

    /**
     * Validação de regra
     * Método: Create billing
     * Regra: Data de limite para desconto precisa ser menor ou igual a data de vencimento da cobrança.
     * Retorno: boleano (true or false)
     *
     * @param $deadlinePaymentDiscount
     * @param $dueDate
     * @return bool
     */
    private function valide_rule_for_deadline_payment_discount_must_be_less_than_or_equal_to_due_date($deadlinePaymentDiscount, $dueDate): bool
    {
        return (Carbon::createFromFormat('Y-m-d', $deadlinePaymentDiscount)->getTimestamp() > Carbon::createFromFormat('Y-m-d', $dueDate)->getTimestamp());
    }
}
