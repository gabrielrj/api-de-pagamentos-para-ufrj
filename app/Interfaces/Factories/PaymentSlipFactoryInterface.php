<?php

namespace App\Interfaces\Factories;

use App\Interfaces\Entities\PaymentInterface;

interface PaymentSlipFactoryInterface
{
    public function getPaymentSlip(array $params, int $billingId): PaymentInterface;
}
